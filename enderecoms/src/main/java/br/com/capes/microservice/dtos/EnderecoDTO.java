package br.com.capes.microservice.dtos;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnderecoDTO {

    private Integer id;
    private Integer idPessoa;
    private String descricao;
}
