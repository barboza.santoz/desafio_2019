package br.com.capes.microservice.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnderecoRepository extends CrudRepository<Endereco, Integer> {

    List<Endereco> findByIdPessoa(Integer idPessoa);

    List<Endereco> findByIdPessoaIn(List<Integer> idsPessoa);

    void deleteByIdPessoa(Integer idPessoa);
}
