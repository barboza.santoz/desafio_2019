package br.com.capes.microservice.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Endereco implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ENDERECO", nullable = false)
    private Integer id;

    @Column(name = "DESC_ENDERECO", nullable = false)
    private String descricao;

    @JoinColumn(name = "ID_PESSOA", nullable = false)
    private Integer idPessoa;
}
