package br.com.capes.microservice.services;

import br.com.capes.microservice.domain.Endereco;
import br.com.capes.microservice.domain.EnderecoRepository;
import br.com.capes.microservice.dtos.EnderecoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class EnderecoService {

    private EnderecoRepository enderecoRepository;

    @Autowired
    public EnderecoService(EnderecoRepository enderecoRepository) {
        this.enderecoRepository = enderecoRepository;
    }

    public Endereco findById(Integer id) {
        Optional<Endereco> optional = enderecoRepository.findById(id);
        return optional.isPresent() ? optional.get() : null;
    }

    public List<Endereco> findAll() {
        return (List) enderecoRepository.findAll();
    }

    public void save(EnderecoDTO enderecoDTO) {
        Endereco endereco = new Endereco();
        endereco.setIdPessoa(enderecoDTO.getIdPessoa());
        endereco.setDescricao(enderecoDTO.getDescricao());
        enderecoRepository.save(endereco);
    }

    public void save(List<EnderecoDTO> enderecosDTO) {
        enderecosDTO.forEach(enderecoDTO -> save(enderecoDTO));
    }

    public void update(EnderecoDTO enderecoForm) {
        Endereco endereco = findById(enderecoForm.getId());
        endereco.setIdPessoa(enderecoForm.getIdPessoa());
        endereco.setDescricao(enderecoForm.getDescricao());
        enderecoRepository.save(endereco);
    }

    public void update(List<EnderecoDTO> enderecosForm) {
        enderecosForm.forEach(endereco -> update(endereco));
    }

    public void delete(Integer id) {
        enderecoRepository.deleteById(id);
    }

    public void deleteByIdPessoa(Integer id) {
        enderecoRepository.deleteByIdPessoa(id);
    }

    public List<Endereco> findByIdPessoa(Integer idPessoa) {
        return enderecoRepository.findByIdPessoa(idPessoa);
    }

    public List<Endereco> findByIdPessoaIn(List<Integer> idsPessoa) {
        return enderecoRepository.findByIdPessoaIn(idsPessoa);
    }
}
