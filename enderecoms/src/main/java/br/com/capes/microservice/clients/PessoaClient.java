package br.com.capes.microservice.clients;

import br.com.capes.microservice.dtos.PessoaDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("pessoams")
public interface PessoaClient {

    @RequestMapping(method= RequestMethod.GET, value="/{idPessoa}/enderecos", consumes="application/json")
    PessoaDTO getEnderecos(@PathVariable("idPessoa") Integer idPessoa);
}

