package br.com.capes.microservice.controllers;

import br.com.capes.microservice.domain.Endereco;
import br.com.capes.microservice.dtos.EnderecoDTO;
import br.com.capes.microservice.services.EnderecoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/endereco")
public class EnderecoController {

    private EnderecoService enderecoService;

    @Autowired
    public EnderecoController(EnderecoService enderecoService) {
        this.enderecoService = enderecoService;
    }

    @ApiOperation(value = "Lista um Endereço por ID")
    @RequestMapping(method = RequestMethod.GET, value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Endereco getById(@PathVariable("id") Integer id) {
        return enderecoService.findById(id);
    }

    @ApiOperation(value = "Lista todos os Enderecos por ID Pessoa")
    @RequestMapping(method = RequestMethod.GET, value="/pessoa/id/{idPessoa}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Endereco> findByIdPessoa(@PathVariable("idPessoa") Integer idPessoa) {
        return enderecoService.findByIdPessoa(idPessoa);
    }

    @ApiOperation(value = "Lista todos os Enderecos por ID's Pessoa")
    @RequestMapping(method = RequestMethod.GET, value="/pessoa/ids", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Endereco> findByIdPessoaIn(@RequestParam("idsPessoa") List<Integer> idsPessoa) {
        return enderecoService.findByIdPessoaIn(idsPessoa);
    }

    @ApiOperation(value = "Lista todos os Endereços")
    @RequestMapping(method = RequestMethod.GET, value="/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Endereco> findAll() {
        return enderecoService.findAll();
    }

    @ApiOperation(value = "Salva um Endereço")
    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> save(@RequestBody EnderecoDTO dto){
        enderecoService.save(dto);
        return new ResponseEntity("Endereço salvo com sucesso", HttpStatus.OK);
    }

    @ApiOperation(value = "Salva vários Endereços")
    @RequestMapping(value = "/saveMany", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveMany(@RequestBody List<EnderecoDTO> enderecos){
        enderecoService.save(enderecos);
        return new ResponseEntity("Endereços salvos com sucesso", HttpStatus.OK);
    }

    @ApiOperation(value = "Altera um Endereço")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> update(@RequestBody EnderecoDTO dto){
        enderecoService.update(dto);
        return new ResponseEntity("Endereço alterado com sucesso", HttpStatus.OK);
    }

    @ApiOperation(value = "Altera vários Endereços")
    @RequestMapping(value = "/updateMany", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateMany(@RequestBody List<EnderecoDTO> enderecos){
        enderecoService.update(enderecos);
        return new ResponseEntity("Endereços alterados com sucesso", HttpStatus.OK);
    }

    @ApiOperation(value = "Deleta um Endereco")
    @RequestMapping(value="/delete/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@PathVariable Integer id){
        enderecoService.delete(id);
        return new ResponseEntity("Endereco deletado com sucesso", HttpStatus.OK);
    }

    @ApiOperation(value = "Deleta um Endereco por ID Pessoa")
    @RequestMapping(value="/pessoa/id/{idPessoa}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteByIdPessoa(@PathVariable Integer idPessoa){
        enderecoService.deleteByIdPessoa(idPessoa);
        return new ResponseEntity("Endereco(s) deletado(s) com sucesso", HttpStatus.OK);
    }
}
