INSERT INTO ENDERECO
(ID_ENDERECO, DESC_ENDERECO,                                ID_PESSOA)
VALUES
(1,           '901 Chapala St, Santa Barbara, 93106',       1),
(2,           '865 Linden Ave, Carpinteria, 93107',         1),
(3,           '37 E. Victoria St, Carpinteria, 93107',      2),
(4,           '6831 Hollister Ave, Goleta, 93108',          2),
(5,           '5995 Stagecoach Rd, San Marcos Pass, 93105', 3),
(6,           '1070 Coast Village Rd, Montecito, 93108',    3);