package br.com.capes.microservice.controller;

import br.com.capes.microservice.controllers.EnderecoController;
import br.com.capes.microservice.domain.Endereco;
import br.com.capes.microservice.services.EnderecoService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static java.util.Arrays.asList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(EnderecoController.class)
public class EnderecoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EnderecoService service;

    @Test
    public void findById__deve_retornar_um_endereco_por_id() throws Exception {

        Endereco endereco = Endereco.builder().id(1).idPessoa(1).descricao("Lorem ipsum dolor").build();

        given(service.findById(1)).willReturn(endereco);

        mockMvc.perform(get("/endereco/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"id\":1,\"descricao\":\"Lorem ipsum dolor\",\"idPessoa\":1}"));
    }

    @Test
    public void findById__deve_retornar_um_endereco_por_idPessoa() throws Exception {

        Endereco endereco1 = Endereco.builder().id(1).idPessoa(1).descricao("Lorem closer dolor ").build();
        Endereco endereco2 = Endereco.builder().id(2).idPessoa(1).descricao("Lorem milper").build();
        Endereco endereco3 = Endereco.builder().id(3).idPessoa(1).descricao("Lorem sharper ipsum miller").build();

        given(service.findByIdPessoa(1)).willReturn(asList(endereco1, endereco2, endereco3));

        mockMvc.perform(get("/endereco/pessoa/id/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"id\":1,\"descricao\":\"Lorem closer dolor \",\"idPessoa\":1},{\"id\":2,\"descricao\":\"Lorem milper\",\"idPessoa\":1},{\"id\":3,\"descricao\":\"Lorem sharper ipsum miller\",\"idPessoa\":1}]"));
    }

    @Test
    public void findById__deve_retornar_um_endereco_por_idPessoa_in() throws Exception {

        Endereco endereco1 = Endereco.builder().id(1).idPessoa(1).descricao("Lorem closer dolor ").build();
        Endereco endereco2 = Endereco.builder().id(2).idPessoa(1).descricao("Lorem milper").build();
        Endereco endereco3 = Endereco.builder().id(3).idPessoa(1).descricao("Lorem sharper ipsum miller").build();

        given(service.findByIdPessoaIn(any())).willReturn(asList(endereco1, endereco2, endereco3));

        mockMvc.perform(get("/endereco/pessoa/ids")
                .queryParam("idsPessoa", "1,2,3")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"id\":1,\"descricao\":\"Lorem closer dolor \",\"idPessoa\":1},{\"id\":2,\"descricao\":\"Lorem milper\",\"idPessoa\":1},{\"id\":3,\"descricao\":\"Lorem sharper ipsum miller\",\"idPessoa\":1}]"));
    }

    @Test
    public void findAll__deve_retornar_todos_os_enderecos() throws Exception {

        Endereco endereco1 = Endereco.builder().id(1).idPessoa(1).descricao("Lorem ipsum dolor").build();
        Endereco endereco2 = Endereco.builder().id(1).idPessoa(1).descricao("Sharper ipsum miller").build();

        given(service.findAll()).willReturn(asList(endereco1, endereco2));

        mockMvc.perform(get("/endereco/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"id\":1,\"descricao\":\"Lorem ipsum dolor\",\"idPessoa\":1},{\"id\":1,\"descricao\":\"Sharper ipsum miller\",\"idPessoa\":1}]"));
    }

    @Test
    public void save__deve_salvar_com_sucesso() throws Exception {
        mockMvc.perform(post("/endereco/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":1,\"descricao\":\"Lorem ipsum dolor\",\"idPessoa\":1}"))
                .andExpect(status().isOk())
                .andExpect(content().string("Endereço salvo com sucesso"));
    }

    @Test
    public void update__deve_alterar_com_sucesso() throws Exception {
        mockMvc.perform(put("/endereco/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":1,\"descricao\":\"Lorem ipsum dolor\",\"idPessoa\":1}"))
                .andExpect(status().isOk())
                .andExpect(content().string("Endereço alterado com sucesso"));
    }

    @Test
    public void delete__deve_deletar_com_sucesso() throws Exception {
        mockMvc.perform(delete("/endereco/delete/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("Endereco deletado com sucesso"));
    }

    @Test
    public void delete__deve_deletar_por_id_pessoa_com_sucesso() throws Exception {
        mockMvc.perform(delete("/endereco/pessoa/id/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("Endereco(s) deletado(s) com sucesso"));
    }
}
