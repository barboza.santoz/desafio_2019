package br.com.capes.microservice.controller;

import br.com.capes.microservice.controllers.PessoaController;
import br.com.capes.microservice.dto.EnderecoDTO;
import br.com.capes.microservice.dto.PessoaDTO;
import br.com.capes.microservice.exception.UsuarioNaoEncontradoExeption;
import br.com.capes.microservice.services.PessoaService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(PessoaController.class)
public class PessoaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PessoaService service;

    @Test
    public void findById__deve_retornar_uma_pessoa_por_id() throws Exception {

        EnderecoDTO endereco = EnderecoDTO.builder().id(1).idPessoa(1).descricao("Lorem ipsum dolor").build();
        PessoaDTO pessoa = PessoaDTO.builder().id(1).nome("sit amet").enderecos(Arrays.asList(endereco)).build();

        given(service.findById(1)).willReturn(pessoa);


        mockMvc.perform(get("/pessoa/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"id\":1,\"nome\":\"sit amet\",\"enderecos\":[{\"id\":1,\"idPessoa\":1,\"descricao\":\"Lorem ipsum dolor\"}]}"));
    }

    @Test
    public void findAll__deve_retornar_todas_as_pessoas() throws Exception {

        EnderecoDTO endereco1 = EnderecoDTO.builder().id(1).idPessoa(1).descricao("Lorem ipsum dolor").build();
        EnderecoDTO endereco2 = EnderecoDTO.builder().id(1).idPessoa(1).descricao("Lorem ipsum dolor").build();
        PessoaDTO pessoa1 = PessoaDTO.builder().id(1).nome("sit amet").enderecos(Arrays.asList(endereco1, endereco2)).build();
        PessoaDTO pessoa2 = PessoaDTO.builder().id(1).nome("plir ajurs").enderecos(Arrays.asList(endereco1, endereco2)).build();

        given(service.findAll()).willReturn(Arrays.asList(pessoa1, pessoa2));


        mockMvc.perform(get("/pessoa/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"id\":1,\"nome\":\"sit amet\",\"enderecos\":[{\"id\":1,\"idPessoa\":1,\"descricao\":\"Lorem ipsum dolor\"},{\"id\":1,\"idPessoa\":1,\"descricao\":\"Lorem ipsum dolor\"}]},{\"id\":1,\"nome\":\"plir ajurs\",\"enderecos\":[{\"id\":1,\"idPessoa\":1,\"descricao\":\"Lorem ipsum dolor\"},{\"id\":1,\"idPessoa\":1,\"descricao\":\"Lorem ipsum dolor\"}]}]"));
    }

    @Test
    public void save__deve_salvar_com_sucesso() throws Exception {
        mockMvc.perform(post("/pessoa/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":1,\"nome\":\"sit amet\",\"enderecos\":[{\"id\":1,\"idPessoa\":1,\"descricao\":\"Lorem ipsum dolor\"}]}"))
                .andExpect(status().isOk())
                .andExpect(content().string("Pessoa salva com sucesso"));
    }

    @Test
    public void update__deve_alterar_com_sucesso() throws Exception {
        mockMvc.perform(put("/pessoa/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":1,\"nome\":\"sit amet\",\"enderecos\":[{\"id\":1,\"idPessoa\":1,\"descricao\":\"Lorem ipsum dolor\"}]}"))
                .andExpect(status().isOk())
                .andExpect(content().string("Pessoa alterada com sucesso"));
    }

    @Test
    public void update__deve_retornar_erro() throws Exception {

        doThrow(new UsuarioNaoEncontradoExeption("Usuário não encontrado")).when(service).update(any());

        mockMvc.perform(put("/pessoa/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Usuário não encontrado"));
    }

    @Test
    public void delete__deve_deletar_com_sucesso() throws Exception {
        mockMvc.perform(delete("/pessoa/delete/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("Pessoa deletada com sucesso"));
    }
}
