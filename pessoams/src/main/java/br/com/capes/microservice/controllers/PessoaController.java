package br.com.capes.microservice.controllers;

import br.com.capes.microservice.dto.PessoaDTO;
import br.com.capes.microservice.exception.UsuarioNaoEncontradoExeption;
import br.com.capes.microservice.services.PessoaService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/pessoa")
public class PessoaController {

    private final PessoaService pessoaService;

    public PessoaController(PessoaService pessoaService) {
        this.pessoaService = pessoaService;
    }

    @ApiOperation(value = "Lista uma Pessoa por ID")
    @RequestMapping(method = RequestMethod.GET, value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public PessoaDTO findById(@PathVariable("id") Integer id) {
        return pessoaService.findById(id);
    }

    @ApiOperation(value = "Lista todas as Pessoas")
    @RequestMapping(method = RequestMethod.GET, value="/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PessoaDTO> findAll() {
        return pessoaService.findAll();
    }

    @ApiOperation(value = "Salva uma Pessoa")
    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> save(@RequestBody PessoaDTO form){
        pessoaService.save(form);
        return new ResponseEntity("Pessoa salva com sucesso", HttpStatus.OK);
    }

    @ApiOperation(value = "Altera uma Pessoa")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> update(@RequestBody PessoaDTO form){
        try {
            pessoaService.update(form);
            return new ResponseEntity("Pessoa alterada com sucesso", HttpStatus.OK);
        } catch (UsuarioNaoEncontradoExeption e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Deleta uma Pessoa")
    @RequestMapping(value="/delete/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@PathVariable Integer id){
        pessoaService.delete(id);
        return new ResponseEntity("Pessoa deletada com sucesso", HttpStatus.OK);
    }
}
