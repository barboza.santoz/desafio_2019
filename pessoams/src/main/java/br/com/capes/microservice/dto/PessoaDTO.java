package br.com.capes.microservice.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PessoaDTO {

    private Integer id;
    private String nome;
    private List<EnderecoDTO> enderecos = new ArrayList<>();
}
