package br.com.capes.microservice.exception;

public class UsuarioNaoEncontradoExeption extends Exception {

    public UsuarioNaoEncontradoExeption(String message) {
        super(message);
    }
}
