package br.com.capes.microservice.clients;

import br.com.capes.microservice.dto.EnderecoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient("enderecoms")
public interface EnderecoClient {

    @RequestMapping(method = RequestMethod.GET, value="/endereco/pessoa/id/{idPessoa}", consumes = MediaType.APPLICATION_JSON_VALUE)
    List<EnderecoDTO> get(@PathVariable("idPessoa") Integer idPessoa);

    @RequestMapping(method = RequestMethod.GET, value="/endereco/pessoa/ids", consumes = MediaType.APPLICATION_JSON_VALUE)
    List<EnderecoDTO> get(@RequestParam("idsPessoa") List<Integer> idsPessoa);

    @RequestMapping(value = "/endereco/saveMany", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    void saveMany(@RequestBody List<EnderecoDTO> enderecos);

    @RequestMapping(value = "/endereco/updateMany", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    void updateMany(@RequestBody List<EnderecoDTO> enderecos);

    @RequestMapping(value="/endereco/pessoa/id/{idPessoa}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteByIdPessoa(@PathVariable("idPessoa") Integer idPessoa);
}

