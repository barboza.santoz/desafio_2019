package br.com.capes.microservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
public class Pessoa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PESSOA", nullable = false)
    private Integer id;

    @Column(name = "DESC_NOME", nullable = false)
    private String nome;

    public Pessoa() {

    }

    public Pessoa(String nome) {
        this.nome = nome;
    }
}
