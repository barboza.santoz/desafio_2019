package br.com.capes.microservice.services;

import br.com.capes.microservice.clients.EnderecoClient;
import br.com.capes.microservice.domain.Pessoa;
import br.com.capes.microservice.domain.PessoaRepository;
import br.com.capes.microservice.dto.EnderecoDTO;
import br.com.capes.microservice.dto.PessoaDTO;
import br.com.capes.microservice.exception.UsuarioNaoEncontradoExeption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
public class PessoaService {

    private EnderecoClient enderecoClient;
    private PessoaRepository pessoaRepository;

    @Autowired
    public PessoaService(EnderecoClient enderecoClient, PessoaRepository pessoaRepository) {
        this.enderecoClient = enderecoClient;
        this.pessoaRepository = pessoaRepository;
    }

    public PessoaDTO findById(Integer id) {
        Optional<Pessoa> optional = pessoaRepository.findById(id);
        if(optional.isPresent()) {
            Pessoa pessoa = optional.get();
            return PessoaDTO.builder()
                    .id(pessoa.getId())
                    .nome(pessoa.getNome())
                    .enderecos(enderecoClient.get(pessoa.getId()))
                    .build();
        }
        return null;
    }

    public List<PessoaDTO> findAll() {

        List<Pessoa> pessoas = (List) pessoaRepository.findAll();
        List<EnderecoDTO> enderecos = enderecoClient.get(pessoas.stream().map(Pessoa::getId).collect(toList()));
        List<PessoaDTO> retorno = new ArrayList<>();

        pessoas.forEach(pessoa -> {
            List<EnderecoDTO> enderecosPorId = enderecos.stream().filter(e -> e.getIdPessoa().equals(pessoa.getId())).collect(toList());
            PessoaDTO dto = PessoaDTO.builder()
                    .id(pessoa.getId())
                    .nome(pessoa.getNome())
                    .enderecos(enderecosPorId)
                    .build();

            retorno.add(dto);
        });

        return retorno;
    }

    public void save(PessoaDTO form) {
        Pessoa pessoa = pessoaRepository.save(new Pessoa(form.getNome()));
        if(!form.getEnderecos().isEmpty()) {
            form.getEnderecos().forEach(endereco -> endereco.setIdPessoa(pessoa.getId()));
            enderecoClient.saveMany(form.getEnderecos());
        }
    }

    public void update(PessoaDTO form) throws UsuarioNaoEncontradoExeption {
        Optional<Pessoa> optional = pessoaRepository.findById(form.getId());
        if(!optional.isPresent()) {
            throw new UsuarioNaoEncontradoExeption("Usuário não encontrado");
        }
        Pessoa pessoa = optional.get();
        pessoa.setNome(form.getNome());
        pessoaRepository.save(pessoa);
        if(!form.getEnderecos().isEmpty()) {
            enderecoClient.updateMany(form.getEnderecos());
        }
    }

    public void delete(Integer id) {
        enderecoClient.deleteByIdPessoa(id);
        pessoaRepository.deleteById(id);
    }
}
